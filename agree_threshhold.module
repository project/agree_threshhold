<?php


/*
 * Imlements entity module hook_field_info
 */
function agree_threshhold_field_info() {
  return array(
    'agree_threshhold' => array(
      'label' => t('Agree threshhold'),
      'description' => t("Records user agreement and creates action when threshhold is reached."),
      'settings' => array('allowed_values' => array(), 'allowed_values_function' => ''),
      'default_widget' => 'agree_threshhold_widget',
      'default_formatter' => 'agree_threshhold_formatter',
      'translatable' => TRUE,
    )
  );
}

/*
 * implements hook_field_widget_info
 */
function agree_threshhold_field_widget_info() {
  return array(
    'agree_threshhold_widget' => array(
      'label' => t('Agree Threshhold'),
      'field types' => array('agree_threshhold'),
    )
  );
}
/*
 * implements hook_field_formatter_info
 */
function agree_threshhold_field_formatter_info() {
  return array(
    'agree_threshhold_formatter' => array(
      'label' => t('Agreement threshhold'),
      'field types' => array('agree_threshhold'),
    )
  );
}

/*
 * implements hook_field_formatter_view
 */
function agree_threshhold_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items) {
  $complete = &$entity->{$field['field_name']}[LANGUAGE_NONE][0]['complete'];
  $type = entity_get_info($entity_type);
  $access = count(array_filter(array_intersect_key($instance['widget']['settings']['roles'], $GLOBALS['user']->roles)));
  $entity_id = $entity->{$type['entity keys']['id']};
  $threshhold = isset($items[0]['threshhold']) ? $items[0]['threshhold'] : 0;
  if (!$access || $complete) {
    return array(
      '#theme' => 'agree_field_closed',
      '#agreers' => count(_agreement_count($entity_type, $entity_id)),
      '#threshhold' => $threshhold,
    );
  }
  return agree_threshhold_field_view($entity_type, $entity_id, $threshhold, FALSE);
}

/*
 * implements hook_theme
 */
function agree_threshhold_theme() {
  return array(
    'agree_field_open' => array(
      'variables' => array(
        'agreers' => '',
        'threshhold' => 0,
        'agreed' => 0,
        'entity_type' => 0,
        'entity_id' => 0
      )
    ),
    'agree_field_closed' => array(
      'variables' => array(
        'agreers' => '',
        'threshhold' => 0,
      )
    )
  );
}

function agree_threshhold_field_view($entity_type, $entity_id, $threshhold, $toggle = TRUE) {
  if ($toggle) agree_threshhold_toggle($entity_type, $entity_id);
  $agreements = _agreement_count($entity_type, $entity_id);

  return array(
    '#theme' => 'agree_field_open',
    '#agreers' => count($agreements),
    '#threshhold' => $threshhold,
    '#agreed' => in_array($GLOBALS['user']->uid, $agreements),
    '#entity_type' => $entity_type,
    '#entity_id' => $entity_id
  );
}

function _agreement_count($entity_type, $entity_id) {
  return db_query("SELECT uid FROM {agreement} WHERE entity_type = :entity_type AND entity_id = :entity_id",
    array(':entity_type' => $entity_type, ':entity_id' => $entity_id)
  )->fetchCol();
}


function theme_agree_field_open($variables) {
  $output = t('@quant agreements towards a threshhold of @num', array(
    '@quant' => $variables['agreers'], '@num' => $variables['threshhold'])
  );
  $link = array(
    '#type' => 'link',
    '#title' => $variables['agreed'] ? t('Un-agree') : t('Agree'),
    '#href' => '',
    '#ajax' => array(
      'path' => 'ajax/agree/'.$variables['entity_type'].'/'.$variables['entity_id'] .'/'. $variables['threshhold'],
      'wrapper' => 'agree-field',
    )
  );
  return '<div id = "agree-field">'.$output.'<br />'.drupal_render($link).'</div>';
}

function theme_agree_field_closed($variables) {
  return t('Threshhold of @num agreements reached', array(
    '@num' => $variables['threshhold'])
  );
}

/*
 * implements hook_field_widget_settings_form
 * no special settings to add.
 */
function agree_threshhold_field_widget_settings_form($field, $instance) {
  $form = array(
    'roles' => array(
      '#title' => t('Roles which can agree'),
      '#type' => 'checkboxes',
      '#options' => array_map('check_plain', user_roles(TRUE)),
      '#default_value' => isset($instance['widget']['settings']['roles']) ? $instance['widget']['settings']['roles'] : array()
    ),
    'mail_subject' => array(
      '#title' => t('Mail subject'),
      '#description' => t('The message that should be sent. You may include placeholders like [node:title], [user:name], and [comment:body] to represent data that will be different each time message is sent. Not all placeholders will be available in all contexts.'),
      //string is borrowed from system_send_email_action_form
      '#type' => 'textfield',
      '#default_value' => isset($instance['widget']['settings']['mail_subject']) ? $instance['widget']['settings']['mail_subject'] : '',
      '#weight' => 1,
    ),
    'mail_body' => array(
      '#title' => t('Mail body'),
      //string is borrowed from system_send_email_action_form
      '#type' => 'textarea',
      '#default_value' => isset($instance['widget']['settings']['mail_body']) ? $instance['widget']['settings']['mail_body'] : '',
      '#rows' => 5,
      '#weight' => 2,
    )
  );

  $agree_threshhold_actions = module_invoke_all('agree_threshhold_actions');
  if (count($agree_threshhold_actions)) {
    $form['action'] = array(
      '#title' => t('Threshhold action'),
      '#description' => t('Action to take when threshhold is reached'),
      '#type' => count($agree_threshhold_actions) > 1 ? 'radios' : 'hidden',
      '#options' => $agree_threshhold_actions,
      '#default_value' => isset($item['action']) ? $item['action'] : '',
      '#weight' => 1
    );
  }
  return $form;
}

/*
 * implements hook_field_widget_form
 */
function agree_threshhold_field_widget_form($form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $item = &$items[0];
  return array(
    '#title' => t('Agreement'),
    '#type' => 'fieldset',
    'threshhold' => array(
      '#title' => t('Threshhold'), //this is only ever seen on the error message anyway
      '#description' => t('Number of agreements needed'),
      '#type' => 'textfield',
      '#default_value' => isset($item['threshhold']) ? $item['threshhold'] : '',
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
      '#size' => 4,
      '#maxlength' => 6,
    )
  );
  return $element;
}

/*
 * implements hook_form_field_ui_field_edit_form_alter
 * remove the default 'worth' setting from the field settings form
 */
function agree_threshhold_form_field_ui_field_edit_form_alter(&$form, $form_state) {
  if ($form['#field']['type'] != 'agree_threshhold') return;
  $form['field']['#access'] = FALSE;
}
/*
 * implements hook_hook_info
 *
 */
function agree_threshhold_hook_info() {
  return array(
    //TODO: a default implementation of this 
    'agree_threshhold_reached' => array()
  );
}

/*
 * implements hook_field_is_empty
 * tests to see if a field is empty
 * $item can contain either the raw field data i.e. integer and division or $quantity
 */
function agree_threshhold_field_is_empty($item) {
  return empty($item['threshhold']);
}

function agree_threshhold_menu() {
  return array(
    'ajax/agree/%/%/%' => array(
      'page callback' => 'agree_threshhold_field_view',
      'page arguments' => array(2, 3, 4),
      'delivery callback' => 'ajax_deliver',
      'access callback' => TRUE
    )
  );
}

/*
 * form definition for agreement button
 */
function agree_threshhold_toggle($entity_type, $entity_id) {
  //flip
  $args = array(
    ':entity_type' => $entity_type,
    ':entity_id' => $entity_id,
    ':uid' => $GLOBALS['user']->uid,
  );
  $agreed = db_query("SELECT TRUE FROM {agreement} WHERE entity_type = :entity_type AND entity_id = :entity_id AND uid = :uid", $args)->fetchfield();

  if ($agreed) {
    db_query("DELETE FROM {agreement} WHERE entity_type = :entity_type AND entity_id = :entity_id AND uid = :uid", $args);
  }
  else {
    db_query("INSERT INTO {agreement} (entity_type, entity_id, uid) VALUES (:entity_type, :entity_id, :uid)", $args);
  }
}


/*
 * implements hook_cron
 */
function agree_threshhold_cron() {
  //for every instance of every field of type 'agree_threshhold'
  $instances = db_query("SELECT fci.field_name, fci.entity_type, fci.bundle
    FROM {field_config} fc
    LEFT JOIN field_config_instance fci
    ON fc.id = fci.field_id
    WHERE fc.type = 'agree_threshhold'")->fetchAll();

  foreach ($instances as $instance) {
    $table = 'field_data_'.$instance->field_name;
    $threshhold_field = $instance->field_name.'_threshhold';
    $complete_field = $instance->field_name.'_complete';
    $query = "SELECT entity_id, $threshhold_field as threshhold
      FROM {$table}
      WHERE entity_type = '$instance->entity_type' AND $complete_field = 0";
    $questions = db_query($query)->fetchAll();
    foreach ($questions as $question) {
      $agreers = db_query(
        "SELECT uid FROM {agreement} WHERE entity_type = :entity_type and entity_id = :entity_id",
        array('entity_type' => $instance->entity_type, ':entity_id' => $question->entity_id)
      )->fetchCol();
      if (count($agreers) >= $question->threshhold) {
        db_query("UPDATE {$table} SET $complete_field = 1 WHERE entity_type = '$instance->entity_type' AND entity_id = $question->entity_id");
        $instance = field_info_instance($instance->entity_type, $instance->field_name, $instance->bundle);
        threshhold_notify_agreers(
          $instance,
          $question->entity_id,
          $agreers
        );
        foreach (module_invoke_all('agree_threshhold_actions') as $callback) {
          $callback($instance, $question->entity_id, $agreers);
        }
      }
    }
  }
}

function threshhold_notify_agreers($instance, $entity_id, $uids) {
  if (empty($instance['widget']['settings']['mail_body'])) return;
  $accounts = user_load_multiple($uids);
  foreach ($accounts as $account) {
    $mails[] = $account->mail;
  }
  $params = array(
    'instance' => $instance,
    'entity_id' => $entity_id
  );
  global $language;
  drupal_mail('agree_threshhold', 'threshhold_reached', implode(',', $mails), $language, $params);
}

function agree_threshhold_mail($key, &$message, $params) {
  //assume the key is threshhold_reached

  $entity = current(entity_load($params['instance']['entity_type'], array($params['entity_id'])));
  $context = array($params['instance']['entity_type'] => $entity);
  $message['subject'] = token_replace($params['instance']['widget']['settings']['mail_subject'], $context);
  $message['body'] = token_replace($params['instance']['widget']['settings']['mail_body'], $context);
  $message['headers']['cc'] = $message['to'];
  $message['to'] = variable_get('site_mail', 'info@example.com');
}